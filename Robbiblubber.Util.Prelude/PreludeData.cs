﻿using System.IO;



namespace Robbiblubber.Util.Prelude
{
    /// <summary>This class implements support for preluded data files.</summary>
    public static class PreludedData
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Extracts a preluded file.</summary>
        /// <param name="sourceFile">Source file name.</param>
        /// <param name="targetFile">Target file name.</param>
        /// <param name="prelude">Prelude.</param>
        /// <returns>Returns TRUE if the file has been successfully extracted, otherwise returns false.</returns>
        public static bool ExtractFromFile(string sourceFile, string targetFile, Prelude prelude = null)
        {
            int i;

            if(prelude == null) { prelude = new Prelude(); }

            FileStream re = File.Open(sourceFile, FileMode.Open);

            while((i = re.ReadByte()) != -1)
            {
                if(prelude._PreludeComplete((byte) i))
                {
                    FileStream wr = File.Open(targetFile, FileMode.Create);
                    _CopyStream(re, wr);
                    wr.Close();
                }
            }
            re.Close();

            return (i > -1);
        }


        /// <summary>Extracts a preluded file.</summary>
        /// <param name="sourceFile">Source file name.</param>
        /// <param name="targetFile">Target file name.</param>
        /// <param name="prelude">Prelude.</param>
        /// <returns>Returns TRUE if the file has been successfully extracted, otherwise returns false.</returns>
        public static bool ExtractFromFile(string sourceFile, string targetFile, byte[] prelude)
        {
            return ExtractFromFile(sourceFile, targetFile, new Prelude(prelude));
        }


        /// <summary>Appends a preluded file to a file.</summary>
        /// <param name="sourceFile">Source file name (file to append).</param>
        /// <param name="targetFile">Target file name (file to append to).</param>
        /// <param name="prelude">Prelude.</param>
        /// <exception cref="InvalidDataException">Thrown when a file already contains prelude data.</exception>
        public static void AppendToFile(string sourceFile, string targetFile, Prelude prelude = null)
        {
            if(prelude == null) { prelude = new Prelude(); }

            _TestPrelude(targetFile, prelude);

            FileStream re = File.Open(sourceFile, FileMode.Open);
            FileStream wr = File.Open(targetFile, FileMode.Append);

            wr.Write(prelude._Prelude, 0, prelude._Prelude.Length);
            _CopyStream(re, wr);

            re.Close();
            wr.Close();
        }


        /// <summary>Appends a preluded file to a file.</summary>
        /// <param name="sourceFile">Source file name (file to append).</param>
        /// <param name="targetFile">Target file name (file to append to).</param>
        /// <param name="prelude">Prelude.</param>
        /// <exception cref="InvalidDataException">Thrown when a file already contains prelude data.</exception>
        public static void AppendToFile(string sourceFile, string targetFile, byte[] prelude)
        {
            AppendToFile(sourceFile, targetFile, new Prelude(prelude));
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static methods                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Tests a target file for existing prelude.</summary>
        /// <param name="targetFile">Target file name.</param>
        /// <param name="prelude">Prelude.</param>
        /// <exception cref="InvalidDataException">Thrown when a file already contains prelude data.</exception>
        private static void _TestPrelude(string targetFile, Prelude prelude)
        {
            if(File.Exists(targetFile))
            {
                FileStream s = File.Open(targetFile, FileMode.Open);
                int i;
                bool ok = true;

                while((i = s.ReadByte()) != -1)
                {
                    if(prelude._PreludeComplete((byte) i))
                    {
                        ok = false;
                        break;
                    }
                }
                s.Close();

                if(!ok) { throw new InvalidDataException("File already contains prelude data."); }
            }
        }


        /// <summary>Reads a stream and appends it to a file.</summary>
        /// <param name="source">Source stream.</param>
        /// <param name="target">Target stream.</param>
        private static void _CopyStream(Stream source, Stream target)
        {
            int rcnt;
            byte[] buf = new byte[2048];

            while((rcnt = source.Read(buf, 0, buf.Length)) > 0)
            {
                target.Write(buf, 0, rcnt);
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [class] Prelude                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Prelude class.</summary>
        public class Prelude
        {
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // private members                                                                                              //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Prelude sequence.</summary>
            internal byte[] _Prelude;

            /// <summary>Current position in prelude.</summary>
            private int _Position = 0;



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // constructors                                                                                                 //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Creates a new instance of this class.</summary>
            public Prelude()
            {
                _Prelude = new byte[24];

                int i;
                for(i =  0; i <  4; i++) { _Prelude[i] = 4; }
                for(i =  4; i <  7; i++) { _Prelude[i] = 7; }
                for(i =  7; i < 13; i++) { _Prelude[i] = 4; }
                _Prelude[13] = 2;
                _Prelude[14] = 4;
                for(i = 15; i < 17; i++) { _Prelude[i] = 8; }
                for(i = 17; i < 23; i++) { _Prelude[i] = 4; }
                _Prelude[22] = 24;
                _Prelude[23] = 4;
            }


            /// <summary>Creates a new instance of this class.</summary>
            /// <param name="prelude">Prelude sequence.</param>
            public Prelude(byte[] prelude)
            {
                _Prelude = prelude;
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // private methods                                                                                              //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Advances prelude reading and returns if the prelude is complete.</summary>
            /// <param name="b">Currently read byte.</param>
            /// <returns>Returns TRUE if the prelude is complete, otherwise returns false.</returns>
            internal bool _PreludeComplete(byte b)
            {
                if(_Prelude[_Position] == b)
                {
                    _Position++;
                }
                else { _Position = 0; }

                return (_Position == _Prelude.Length);
            }
        }
    }
}
